
<!-- README.md is generated from README.Rmd. Please edit that file -->

# libminer

<!-- badges: start -->
<!-- badges: end -->

The goal of libminer is to provide an overview of your R library. It is
a toy package from a workshop and not meant for serious use.

## Installation

You can install the development version of libminer like so:

``` r
devtools::install_gitlab("hayward/libminer")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(libminer)
lib_summary()
#>                                                                             Library
#> 1                                                C:/Program Files/R/R-4.2.3/library
#> 2                         C:/Users/hayward.LANGROUP/AppData/Local/R/win-library/4.2
#> 3 C:/Users/hayward.LANGROUP/AppData/Local/Temp/RtmpohAKv6/temp_libpath1b7d8318d22b7
#>   n_packages
#> 1         30
#> 2        253
#> 3          1

#or to calculate sizes:
lib_summary(sizes=TRUE)
#>                                                                             Library
#> 1                                                C:/Program Files/R/R-4.2.3/library
#> 2                         C:/Users/hayward.LANGROUP/AppData/Local/R/win-library/4.2
#> 3 C:/Users/hayward.LANGROUP/AppData/Local/Temp/RtmpohAKv6/temp_libpath1b7d8318d22b7
#>   n_packages  lib_size
#> 1         30  67242362
#> 2        253 875991256
#> 3          1     12853
```
